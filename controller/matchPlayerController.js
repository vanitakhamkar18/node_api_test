const {Player,validate} =  require('../models/player.model');
const {Country,validateCountry} =  require('../models/country.model');
const {Team} =  require('../models/team.model');
const {Match} =  require('../models/match.model');

// add , edit ,get, delete player
exports.get_all = async (req,res) =>
{
    const pageNum = req.query.pageNo ? req.query.pageNo : 1;
    const pageSize = 10;
    const player = await Player
                    .find()
                    .skip((pageSize *(pageNum-1)))
                    .select('_id name country age date')
                    .populate({ path: 'country', select: 'name' })
                    .where('is_active').equals(0)
                    .limit(pageSize)
                    .sort('name');
    if(player.length > 0 && player != null && player != []){
        const response = {
            count : player.length,
            result : player.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    age : doc.age,
                    country : doc.country,
                    date : doc.date,
                    request :{
                        type : 'GET',
                        url : 'http://localhost/:3000/player/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    }else{
        res.status(404).json({
            msg : 'No entry Found'
        });
    }
};

exports.get_player = async (req,res) =>
{
    const id = req.params.id;
    try{
        const player = await Player.findById(id);
        if (!player) return res.status(404).send('The player with the given ID was not found.');
        const response = {
            error : 0,
            id : player._id,
            name : player.name,
            age : player.age,
            type : player.type,
            image : player.image,
            country : player.country,
            date : player.date
        };
        res.status(200).json(response);
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


exports.add_player = async (req,res) =>
{
    const { error } = validate(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    let player = new Player({
        name: req.body.name,
        age: req.body.age,
        image: req.file.filename,
        country: req.body.country,
        type: req.body.type,
    });
    try{
        player = await player.save();
        const response = {
            error:0,
            id : player._id,
            name : player.name,
            age : player.age,
            type : player.type,
            country : player.country,
            image : 'http://localhost:3000/uploads/'+player.image,
            date : player.date,
            request : {
                type : 'GET',
                url : 'http://localhost:3000/player/get/'+player._id
            }
        };
        res.status(200).json(response);
    }
    catch(ex){
        res.status(400).send(ex.errors);
    }
};


exports.edit_player = async (req,res) =>
{
    try{
        const updatePlayer = await Player.updateOne(
            { _id: req.params.id },
            { $set: req.body }
            );
        res.status(200).json({"error":0,"msg":"Player updated successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

exports.delete_player = async (req,res) =>
{
    try{
        const updatePlayer = await Player.updateOne(
            { _id: req.params.id },
            { $set: {is_active : 1} }
            );
        res.status(200).json({"error":0,"msg":"Player Deleted successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};



// add , edit ,get, delete country


exports.get_all_country = async (req,res) =>
{

    const country = await Country
                    .find()
                    .select('_id name date')
                    .where('is_active').equals(0)
                    .sort('name');
    if(country.length > 0 && country != null && country != []){
        const response = {
            count : country.length,
            result : country.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    date : doc.date,
                    request :{
                        type : 'GET',
                        url : 'http://localhost:3000/country/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    }else{
        res.status(404).json({
            msg : 'No entry Found'
        });
    }
};

exports.get_country = async (req,res) =>
{
    const id = req.params.id;
    try{
        const country = await Country.findById(id);
        if (!country) return res.status(404).send('The country with the given ID was not found.');
        const response = {
            error : 0,
            id : country._id,
            name : country.name,
            date : country.date
        };
        res.status(200).json(response);
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


exports.add_country = async (req,res) =>
{    console.log(req.body)
    const { error } = validateCountry(req.body);
    if(error) return res.status(400).send(error.details[0].message);
    let country = new Country({
        name: req.body.name
    });
    try{
        country = await country.save();
        const response = {
            error:0,
            id : country._id,
            name : country.name,
            date : country.date,
            request : {
                type : 'GET',
                url : 'http://localhost:3000/country/get/'+country._id
            }
        };
        res.status(200).json(response);
    }
    catch(ex){
        res.status(400).send(ex.errors);
    }
};


exports.edit_country = async (req,res) =>
{
    try{
        const updateCountry = await Country.updateOne(
            { _id: req.params.id },
            { $set: req.body }
            );
        res.status(200).json({"error":0,"msg":"Country updated successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

exports.delete_country = async (req,res) =>
{
    try{
        const updateCountry = await Country.updateOne(
            { _id: req.params.id },
            { $set: {is_active : 1} }
            );
        res.status(200).json({"error":0,"msg":"Country Deleted successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};




// add , edit ,get, delete team


exports.get_all_team = async (req,res) =>
{
    const team = await Team
                    .find()
                    .select('_id name date country players')
                    .populate({ path: 'country', select: 'name' })
                    .populate({ path: 'players', select: 'name' })
                    .where('is_active').equals(0)
                    .sort('name');
    if(team.length > 0 && team != null && team != []){
        const response = {
            count : team.length,
            result : team.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    date : doc.date,
                    country : doc.country,
                    players : doc.players,
                    request :{
                        type : 'GET',
                        url : 'http://localhost:3000/team/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    }else{
        res.status(404).json({
            msg : 'No entry Found'
        });
    }
};

exports.get_team = async (req,res) =>
{
    const id = req.params.id;
    try{
        const team = await Team.findById(id);
        if (!team) return res.status(404).send('The team with the given ID was not found.');
        const response = {
            error : 0,
            id : team._id,
            name : team.name,
            date : team.date
        };
        res.status(200).json(response);
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


exports.add_team = async (req,res) =>
{
    try{
        let team = new Team({
            name: req.body.name,
            country : req.body.country,
            players : req.body.players
        });
        team = await team.save();
        const response = {
            error:0,
            id : team._id,
            date : team.date,
            players : team.players,
            request : {
                type : 'GET',
                url : 'http://localhost:3000/team/get/'+team._id
            }
        };
        res.status(200).json(response);
    }
    catch(ex){
        res.status(400).send(ex.errors);
    }
};


exports.edit_team = async (req,res) =>
{
    try{
        const updateTeam = await Team.updateOne(
            { _id: req.params.id },
            { $set: req.body }
            );
        res.status(200).json({"error":0,"msg":"Team updated successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

exports.delete_team = async (req,res) =>
{
    try{
        const updateTeam = await Team.updateOne(
            { _id: req.params.id },
            { $set: {is_active : 1} }
            );
        res.status(200).json({"error":0,"msg":"Team Deleted successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};





// add , edit ,get, delete match

exports.get_all_match = async (req,res) =>
{
    const match = await Match
                    .find()
                    .select('_id name date teams')
                    .populate({ path: 'teams', select: 'name' })
                    .where('is_active').equals(0)
                    .sort('name');
    if(match.length > 0 && match != null && match != []){
        const response = {
            count : match.length,
            result : match.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    date : doc.date,
                    teams : doc.teams,
                    request :{
                        type : 'GET',
                        url : 'http://localhost:3000/match/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
    }else{
        res.status(404).json({
            msg : 'No entry Found'
        });
    }
};

exports.get_match = async (req,res) =>
{
    const id = req.params.id;
    try{
        const match = await Match.findById(id);
        if (!match) return res.status(404).send('The match with the given ID was not found.');
        const response = {
            error : 0,
            id : match._id,
            name : match.name,
            teams : match.teams,
            date : match.date
        };
        res.status(200).json(response);
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


exports.add_match = async (req,res) =>
{
    try{
        let match = new Match({
            name: req.body.name,
            teams : req.body.teams
        });
        match = await match.save();
        const response = {
            error:0,
            id : match._id,
            date : match.date,
            teams : match.teams,
            request : {
                type : 'GET',
                url : 'http://localhost:3000/match/get/'+match._id
            }
        };
        res.status(200).json(response);
    }
    catch(ex){
        res.status(400).send(ex.errors);
    }
};


exports.edit_match = async (req,res) =>
{
    try{
        const updateMatch = await Match.updateOne(
            { _id: req.params.id },
            { $set: req.body }
            );
        res.status(200).json({"error":0,"msg":"Match updated successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

exports.delete_match = async (req,res) =>
{
    try{
        const updateMatch = await Match.updateOne(
            { _id: req.params.id },
            { $set: {is_active : 1} }
            );
        res.status(200).json({"error":0,"msg":"Match Deleted successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

// get players by country id for respective countries team

exports.getPlayersByCountry = async (req,res) =>
{
    try{
        let cId = req.params.id;
        const players = await Player
                    .find()
                    .select('_id name age type image')
                    .where('is_active').equals(0)
                    .where('country').equals(cId);
        console.log(players);
       if(players.length > 0 && players != null && players != []){
        const response = {
            error : 0,
            count : players.length,
            result : players.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    age : doc.age,
                    type : doc.type,
                    image : doc.image,
                    request :{
                        type : 'GET',
                        url : 'http://localhost:3000/player/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
        }else{
            res.status(404).json({
                msg : 'No entry Found'
            });
        }
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


// get teams by country id for respective countries matches

exports.getTeamsByCountry = async (req,res) =>
{
    try{
        let cId = req.params.id;
        const team = await Team
                    .find()
                    .select('_id name')
                    .where('is_active').equals(0)
                    .where('country').equals(cId);
        console.log(team);
       if(team.length > 0 && team != null && team != []){
        const response = {
            error : 0,
            count : team.length,
            result : team.map(doc =>{
                return{
                    id : doc._id,
                    name : doc.name,
                    request :{
                        type : 'GET',
                        url : 'http://localhost:3000/team/'+ doc._id
                    }
                }
            })
        };
        res.status(200).json(response);
        }else{
            res.status(404).json({
                msg : 'No entry Found'
            });
        }
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};

// post active match
exports.addActiveMatch = async (req,res) =>
{
    try{
        const deActiveMatch = await Match.updateMany(
            {},
            { $set: {is_active : 1} }
            );

        const updateActiveMatch = await Match.updateOne(
            { _id: req.params.id },
            { $set: {is_active : 0} }
            );
        res.status(200).json({"error":0,"msg":"Match Activeted successfully"});
    }catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};


// get active match and thier team player details

exports.getActiveMatchData = async (req,res) =>
{
    try{
        console.log('---------------');
        const activeMatch = await Match
                    .find()
                    .select('_id name venue description date teams')
                    .populate({ path: 'teams', select: 'name players',
                        populate: {
                           path: 'players',
                           select: 'name'
                         } })
                    .where('is_active').equals(0);

             console.log(activeMatch);
        if(activeMatch.length > 0 && activeMatch != null && activeMatch != []){
             //activeMatch = Ojbect.keys(activeMatch);
            const response = {

                error : 0,
                data : activeMatch.map(doc =>{
                    return{
                        name : doc.name,
                        venue : doc.venue,
                        description : doc.description,
                        date : doc.date,
                        team : doc.teams
                    }
                })

            };
            res.status(200).json(response);
        }else{
            res.status(404).json({
                msg : 'No entry Found'
            });
        }
    }
    catch(err){
        res.status(400).json({ "error":1,"msg":err });
    }
};
