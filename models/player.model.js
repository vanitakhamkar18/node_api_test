const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const playerSchema = new Schema({
    name : {type:String, required:true,minlength:5,maxlength : 50},
    country : {type:mongoose.Schema.Types.ObjectId, ref :"Country"},
    age : {type:Number, required:true },
    type : {type:String, required:true },
    image : {type:String, required:true },
    date : {type : Date , default : Date.now},
    is_active : {type:Number , default : 0}
});


const Player = mongoose.model('Player',playerSchema);

function validatePlayer(player){
    const schema = {
        name : Joi.string().min(5).max(50).required(),
        country : Joi.string().required(),
        age : Joi.number().required(),
        type : Joi.string().required()
    }
    return Joi.validate(player, schema);
}
exports.Player = Player;
exports.validate = validatePlayer;
