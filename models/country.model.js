const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const countrySchema = new Schema({
    name : {type:String, required:true,minlength:5,maxlength : 50},
    date : {type : Date , default : Date.now},
    is_active : {type:Number , default : 0}
});


const Country = mongoose.model('Country',countrySchema);

function validateCountry(country){
    const schema = {
        name : Joi.string().min(5).max(50).required(),
    }
    return Joi.validate(country, schema);
}
exports.Country = Country;
exports.validateCountry = validateCountry;
