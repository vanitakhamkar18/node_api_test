const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const teamSchema = new Schema({
    name : {type:String, required:true,minlength:5,maxlength : 50},
    players : [{ type: Schema.Types.ObjectId, ref: 'Player' }],
    description : {type:String, default : 'None'},
    country : {type:mongoose.Schema.Types.ObjectId, ref :"Country"},
    date : {type : Date , default : Date.now},
    is_active : {type:Number , default : 0}
});


const Team = mongoose.model('Team',teamSchema);

function validateTeam(team){
    const schema = {
        name : Joi.string().min(5).max(50).required(),
    }
    return Joi.validate(team, schema);
}
exports.Team = Team;
exports.validateTeam = validateTeam;
