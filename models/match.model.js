const mongoose = require('mongoose');
const Joi = require('joi');

const Schema = mongoose.Schema;

const matchSchema = new Schema({
    name : {type:String, required:true,minlength:5,maxlength : 50},
    teams : [{ type: Schema.Types.ObjectId, ref: 'Team' }],
    description : {type:String, default : 'None'},
    venue : {type:String, default : 'None'},
    date : {type : Date , default : Date.now},
    is_active : {type:Number , default : 0}
});


const Match = mongoose.model('Match',matchSchema);

exports.Match = Match;
