const createError = require('http-errors');
const express = require('express');
var dbDebugger = require('debug')('app:db');
const cors = require('cors');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const mongoose = require('mongoose');
const Joi = require('joi');
const bodyParser = require('body-parser');
const indexRouter = require('./routes/index');
const countryRouter = require('./routes/country');
const teamRouter = require('./routes/team');
const matchRouter = require('./routes/match');
const playerRouter = require('./routes/player');

const app = express();
// config
require('dotenv').config();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
//app.set('view engine', 'jade');
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use('/uploads',express.static('uploads'));
app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/player', playerRouter);
app.use('/country', countryRouter);
app.use('/team', teamRouter);
app.use('/match', matchRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

//connect DB
console.log(process.env.MONGODB_URL);
const dbUrl = process.env.MONGODB_URL+'CricketLeague';
mongoose.connect(dbUrl,{useNewUrlParser: true,useUnifiedTopology:true})
.then(() => console.log('Connected To Mongo'))
.catch(err=>console.error('Not connected..',err));

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
// var port = process.env.PORT ? process.env.PORT : '9000';

//app.listen(4000, () => console.log(`Express server currently running on port `+4000));

module.exports = app;
