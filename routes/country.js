const express = require('express');
const router = express.Router();
const path = require('path');
const app = express();
const matchPlayer = require('../controller/matchPlayerController');


router.get('/', matchPlayer.get_all_country);

router.get('/:id',matchPlayer.get_country);

router.post("/add", matchPlayer.add_country);

router.patch("/:id", matchPlayer.edit_country);

router.delete("/:id", matchPlayer.delete_country);

module.exports = router;
