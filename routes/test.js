const express = require('express');
const router = express.Router();
const path = require('path');

const app = express();
require('dotenv').config(); 

app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'hbs');
app.set('view engine', 'html');
app.engine('html', require('hbs').__express);


router.get('/', function(req, res, next) {

  res.send('My Test API'+process.env.WEB_URL);
});

router.get("/index", function(req, res, next) {
    res.render("test", {
        title: "You are subscribed",
        body : 'vanita test html'
    });
});


router.get("/add", function(req, res, next) {
    res.render("user");
});

module.exports = router;
