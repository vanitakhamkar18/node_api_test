const express = require('express');
const router = express.Router();
const path = require('path');
const app = express();
require('dotenv').config();
const matchPlayer = require('../controller/matchPlayerController');
const multer = require('multer');

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null,Date.now() +'-'+file.originalname)
  }
});
const filefilter = (req ,file,cb) => {
  if(file.mimetype === 'image/jpeg' || file.mimetype === 'image/png'){
    cb(null,true);
  }else{
    cb(new Error('Invalid File'),false);
  }
}
var upload = multer({
                  storage: storage,
                  limits: {
                    filesize : 1024 * 1024 *5
                  },
                  filefilter : filefilter
                });


router.get('/', matchPlayer.get_all);

router.get('/:id', matchPlayer.get_player);

router.post("/add", upload.single('playerImg'), matchPlayer.add_player);

router.patch("/update/:id", matchPlayer.edit_player);

router.delete("/:id", matchPlayer.delete_player);

module.exports = router;
