const express = require('express');
const router = express.Router();
const path = require('path');
const app = express();
const matchPlayer = require('../controller/matchPlayerController');

router.get('/get-data', matchPlayer.getActiveMatchData);

router.get('/', matchPlayer.get_all_match);

router.get('/:id',matchPlayer.get_match);

router.post("/add", matchPlayer.add_match);

router.patch("/:id", matchPlayer.edit_match);

router.delete("/:id", matchPlayer.delete_match);

router.get('/get-player-by-country/:id', matchPlayer.getPlayersByCountry);

router.get('/get-team-by-country/:id', matchPlayer.getTeamsByCountry);

router.get('/add-active-match/:id', matchPlayer.addActiveMatch);


module.exports = router;
