const express = require('express');
const router = express.Router();
const path = require('path');
const app = express();
const matchPlayer = require('../controller/matchPlayerController');


router.get('/', matchPlayer.get_all_team);

router.get('/:id',matchPlayer.get_team);

router.post("/add", matchPlayer.add_team);

router.patch("/:id", matchPlayer.edit_team);

router.delete("/:id", matchPlayer.delete_team);

module.exports = router;
